package com.example.hungdao1311.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<MyContact> contactsData;
    private Context context;

    public MyAdapter(ArrayList<MyContact> contactsData, Context context) {
        this.contactsData = contactsData;
        this.context = context;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_model, null);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        final MyContact selected = contactsData.get(position);

        viewHolder.tvName.setText(contactsData.get(position).getName());
        viewHolder.tvPhone.setText(contactsData.get(position).getPhoneNumber());
        if (contactsData.get(position).getPhoto() != null) {
            viewHolder.ivPhoto.setImageBitmap(contactsData.get(position).getPhoto());
        } else {
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.monka);
            viewHolder.ivPhoto.setImageBitmap(bm);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onContactClicked(selected);
            }
        });
    }

    private void onContactClicked(MyContact position) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("data", position);
        context.startActivity(intent);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public TextView tvPhone;
        public ImageView ivPhoto;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvName = itemLayoutView.findViewById(R.id.tv_name);
            tvPhone = itemLayoutView.findViewById(R.id.tv_phone);
            ivPhoto = itemLayoutView.findViewById(R.id.iv_photo);
        }
    }

    @Override
    public int getItemCount() {
        return contactsData.size();
    }
}
