package com.example.hungdao1311.myapplication;
import java.io.Serializable;
import java.util.ArrayList;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public  class MyContact implements Serializable {

    private String name;
    private String phoneNumber;
    private String emailID;
    private Bitmap photo;

    public MyContact(String name, String phoneNumber, String mail) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.emailID = mail;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhoneNumber(String number) {
        this.phoneNumber = number;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setEmaiID(String email) {
        this.emailID = email;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setPhoto(Bitmap photo){
        this.photo = photo;
    }

    public Bitmap getPhoto(){
        return photo;
    }
}





