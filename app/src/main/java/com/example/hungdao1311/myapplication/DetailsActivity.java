package com.example.hungdao1311.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        ;
        MyContact contact = (MyContact) intent.getSerializableExtra("data");

        TextView tvName = findViewById(R.id.tv_name);
        TextView tvPhone = findViewById(R.id.tv_phone);
        TextView tvEmail = findViewById(R.id.tv_email);
        ImageView ivPhoto = findViewById(R.id.iv_photo);

        tvName.setText(contact.getName());
        tvPhone.setText(contact.getPhoneNumber());
        tvEmail.setText(contact.getEmailID());
        if (contact.getPhoto() != null) {
            ivPhoto.setImageBitmap(contact.getPhoto());
        } else {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.monka);
            ivPhoto.setImageBitmap(bm);
        }
    }
}
