package com.example.hungdao1311.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting:
                Intent mIntent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(mIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<MyContact> contactsData = new ArrayList<MyContact>();
        contactsData.add(new MyContact("mot", "1111111", "1111@mm.com"));
        contactsData.add(new MyContact("hai", "22222", "22222@mm.com"));
        contactsData.add(new MyContact("ba", "333333", "3333@11.com"));
        contactsData.add(new MyContact("bon", "444444", "444444@m.com"));
        contactsData.add(new MyContact("nam", "55555", "55555@mm.com"));
        contactsData.add(new MyContact("sau", "66666", "666666@mm.com"));

        MyAdapter mAdapter = new MyAdapter(contactsData, this);

        recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


}
